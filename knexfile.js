export default {
  client: "postgres",
  migrations: {
    directory: "./src/lib/migrations",
    extension: "ts",
  },
};
