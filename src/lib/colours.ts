const colours = {
  BLURPLE: 0x7289da,
  RED: 0xe74c3c,
  GOLD: 0xf1c40f,
  ORANGE: 0xe67e22,
  GREEN: 0x2ecc71,
};

export default colours;
