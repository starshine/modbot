import chalk, { ChalkInstance } from "chalk";
import { exit } from "process";

let longestName = 0;

function padName(name: string): string {
  if (longestName === name.length) {
    return name;
  }

  if (longestName - (name.length % 2) === 0) {
    const pad = (longestName - name.length) / 2;

    return " ".repeat(pad) + name + " ".repeat(pad);
  }

  const beforePad = Math.floor((longestName - name.length) / 2);
  const afterPad = longestName - name.length - beforePad;

  return " ".repeat(beforePad) + name + " ".repeat(afterPad);
}

function log(lvl: string, msg: string, name: string | null = null): string {
  let str = `${chalk.gray("[")}${chalk.white(
    new Date().toLocaleTimeString("en-GB", {
      hour12: false,
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    })
  )}${chalk.gray("]")}`;

  switch (lvl) {
    case "debug":
      str += ` ${chalk.gray("[")}${chalk.magenta.bold("DBG")}${chalk.gray(
        "]"
      )} `;
      break;
    case "info":
      str += ` ${chalk.gray("[")}${chalk.blue.bold("INF")}${chalk.gray("]")} `;
      break;
    case "warn":
      str += ` ${chalk.gray("[")}${chalk.yellow.bold("WRN")}${chalk.gray(
        "]"
      )} `;
      break;
    case "error":
      str += ` ${chalk.gray("[")}${chalk.red.bold("ERR")}${chalk.gray("]")} `;
      break;
    case "fatal":
      str += ` ${chalk.gray("[")}${chalk.red.bold("FTL")}${chalk.gray("]")} `;
      break;
    default:
      if (lvl.length === 3) {
        str += ` ${chalk.gray("[")}${chalk.cyan(lvl.toUpperCase())}${chalk.gray(
          "]"
        )} `;
      } else if (lvl.length < 3) {
        str += ` ${chalk.gray("[")}${chalk.cyan(
          lvl.padEnd(3, " ").toUpperCase()
        )}${chalk.gray("]")} `;
      } else {
        str += ` ${chalk.gray("[")}${chalk.cyan(
          lvl.slice(0, 3).toUpperCase()
        )}${chalk.gray("]")} `;
      }
      break;
  }

  if (name !== null) {
    str += `${chalk.gray("[")}${chalk.bold(padName(name))}${chalk.gray("]")} `;
  } else if (longestName !== 0) {
    str += `${chalk.gray("[")}${chalk.bold(
      "".padEnd(longestName + 2, " ")
    )}${chalk.gray("]")} `;
  }

  return str + msg;
}

export interface Logger {
  _chalk: ChalkInstance;

  _log(lvl: string, msg: string, name: string | null): string;

  debug(msg: string): void;
  info(msg: string): void;
  warn(msg: string): void;
  error(msg: string): void;
  fatal(msg: string): void;
}

export function named(name: string | null): Logger {
  if ((name?.length || 0) > longestName) {
    longestName = name!.length;
  }

  return {
    _chalk: chalk,
    _log: log,

    debug: (msg: string) => console.log(log("debug", msg, name)),
    info: (msg: string) => console.log(log("info", msg, name)),
    warn: (msg: string) => console.log(log("warn", msg, name)),
    error: (msg: string) => console.log(log("error", msg, name)),

    fatal: (msg: string) => {
      console.log(log("fatal", msg, name));
      exit(1);
    },
  };
}

export default named(null);
