import {
  ShardClient,
  ShardClientOptions,
  Structures,
  GatewayClientEvents,
  Constants,
} from "detritus-client";
import { RequestTypes } from "detritus-client-rest";
import { Embed } from "detritus-client/lib/utils/index.js";
import {
  Channel,
  Guild,
  Member,
  Message,
  MessageEmbed,
  User,
} from "detritus-client/lib/structures";
import { GatewayIntents } from "detritus-client-socket/lib/constants.js";
import { RestClient } from "detritus-client/lib/rest";

import { v4 as uuid } from "uuid";

import { ChannelNotFound, NotCommandError } from "./errors.js";
import { named } from "./log.js";
import colours from "./colours.js";

import db, { Database } from "./db.js";
import { GuildConfig } from "./tables.js";
import { reactionHandler, addReactionHandler } from "./HandlerManager.js";

const log = named("bot");
const intents =
  GatewayIntents.DIRECT_MESSAGES |
  GatewayIntents.DIRECT_MESSAGE_REACTIONS |
  GatewayIntents.GUILDS |
  GatewayIntents.GUILD_BANS |
  GatewayIntents.GUILD_EMOJIS |
  GatewayIntents.GUILD_INVITES |
  GatewayIntents.GUILD_MEMBERS |
  GatewayIntents.GUILD_MESSAGES |
  GatewayIntents.GUILD_MESSAGE_REACTIONS |
  GatewayIntents.GUILD_PRESENCES |
  GatewayIntents.GUILD_VOICE_STATES;

interface BotOptions extends ShardClientOptions {
  prefixes: Array<string>;
  owners?: Array<string>;
}

export interface Command {
  name: string;
  aliases?: Array<string>;
  summary: string;
  description?: string;

  guildOnly?: boolean;

  helperOnly?: boolean;
  modOnly?: boolean;
  ownerOnly?: boolean;

  // eslint-disable-next-line no-use-before-define
  run(
    ctx: Context
  ): Promise<
    | string
    | Structures.MessageEmbed
    | Embed
    | Array<Structures.MessageEmbed>
    | Array<Embed>
    | null
    | void
    | undefined
  >;

  subCommands?: Array<Command>;
}

export class Bot extends ShardClient {
  prefixes: Array<string>;

  private _owners: Array<string>;

  private _commands: Map<string, Command>;

  constructor(token: string, options: BotOptions | undefined) {
    if (!options) {
      options = {
        prefixes: ["."],
        gateway: {
          intents: intents,
        },
      };
    } else if (!options.gateway?.intents) {
      if (!options.gateway) {
        options.gateway = {
          intents: intents,
        };
      } else {
        options.gateway.intents = intents;
      }
    }

    super(token, options);

    this.prefixes = options?.prefixes || ["."];

    this._commands = new Map<string, Command>();

    this._owners = options?.owners || [];

    this.on("messageCreate", this._onMessage);
    this.on("messageReactionAdd", this._onReactionAdd);

    this.on("gatewayReady", (ev: GatewayClientEvents.GatewayReady) => {
      log.info(
        `Logged in as ${ev.raw.user.username}#${ev.raw.user.discriminator} (${ev.raw.user.id})!`
      );
    });
  }

  get commands() {
    return this._commands;
  }

  public getRootChannel(id: string): Structures.Channel | null {
    let channel = this.channels.get(id);
    if (!channel) return null;

    const { ChannelTypes } = Constants;

    switch (channel.type) {
      case ChannelTypes.GUILD_NEWS_THREAD:
      case ChannelTypes.GUILD_PUBLIC_THREAD:
      case ChannelTypes.GUILD_PRIVATE_THREAD:
        channel = this.channels.get(channel.parentId!);
    }

    return channel ?? null;
  }

  private async _onMessage(ev: GatewayClientEvents.MessageCreate) {
    if (ev.message.author.bot) return;

    try {
      const ctx = new Context(this, ev);

      const cmd = this._commands.get(ctx.rootCommand);
      if (!cmd) return;

      await this._runCommand(ctx, cmd);
    } catch (e) {
      if (e === NotCommandError) return;

      await this._logError(ev, e);
    }
  }

  private async _onReactionAdd(ev: GatewayClientEvents.MessageReactionAdd) {
    const handler = reactionHandler.get({
      userId: ev.userId,
      messageId: ev.messageId,
      emoji: ev.reaction.emoji.format,
    });
    if (!handler) return;

    if (handler.removeOnUse) {
      reactionHandler.delete({
        userId: ev.userId,
        messageId: ev.messageId,
        emoji: ev.reaction.emoji.format,
      });
    }

    await handler.run(ev);
  }

  private async _runCommand(ctx: Context, cmd: Command): Promise<void> {
    // quick sanity check
    if (!cmd) return;

    if (ctx.args?.length !== 0 && cmd.subCommands) {
      for (const subCmd of cmd.subCommands) {
        if (
          subCmd.name.toLowerCase() === ctx.args![0].toLowerCase() ||
          subCmd.aliases?.some(
            (alias) => alias.toLowerCase() === ctx.args![0].toLowerCase()
          )
        ) {
          return await this._runCommand(ctx, subCmd);
        }
      }
    }

    if (cmd.ownerOnly && !this._owners.some((id) => id === ctx.author.id)) {
      await ctx.reply("Only the bot owner can use this command.", colours.RED);
      return;
    }

    if (!ctx.guild && (cmd.guildOnly || cmd.helperOnly || cmd.modOnly)) {
      await ctx.reply("This command cannot be run in DMs.", colours.RED);
      return;
    }

    if (cmd.helperOnly || cmd.modOnly) {
      // admins override perms no matter what
      if (!(ctx.author as Structures.Member).canAdministrator) {
        const guildConfig = await ctx.getGuildConfig();

        if (cmd.modOnly) {
          if (!guildConfig!.modRole) {
            await ctx.reply(
              "This command requires the mod role, but no mod role is set for this guild.",
              colours.RED
            );
            return;
          }

          const hasPerms = (ctx.author as Structures.Member)._roles?.some(
            (id) => id === guildConfig!.modRole
          );

          if (!hasPerms) {
            await ctx.reply(
              `This command requires the mod role <@&${
                guildConfig!.modRole
              }>, but you don't have it.`,
              colours.RED
            );
            return;
          }
        } else if (cmd.helperOnly) {
          if (!guildConfig!.modRole && !guildConfig!.helperRole) {
            await ctx.reply(
              "This command requires the helper role, but no helper role is set for this guild.",
              colours.RED
            );

            const hasPerms = (ctx.author as Structures.Member)._roles?.some(
              (id) => id === guildConfig!.helperRole
            );

            if (!hasPerms) {
              await ctx.reply(
                `This command requires the helper role <@&${
                  guildConfig!.helperRole
                }>, but you don't have it.`,
                colours.RED
              );
              return;
            }
          }
        }
      }
    }

    await this.sendResponse(ctx.channel.id, cmd.run(ctx));
  }

  public async sendResponse(
    channelId: string,
    input: Promise<
      | string
      | Structures.MessageEmbed
      | Embed
      | Array<Structures.MessageEmbed>
      | Array<Embed>
      | null
      | void
      | undefined
    >
  ): Promise<void> {
    const response = await input;
    if (!response) return;

    const data: RequestTypes.CreateMessage = {
      allowedMentions: {},
    };
    if (typeof response === "string") {
      data.content = response as string;
    } else {
      data.embeds = response as Structures.MessageEmbed[];
    }

    await this.rest.createMessage(channelId, data);
  }

  private async _logError(ev: GatewayClientEvents.MessageCreate, err: any) {
    const id = uuid();

    log.error(`Error ${log._chalk.red(id)}: ${err}`);

    try {
      const embed = new Embed()
        .setColor(colours.RED)
        .setTimestamp(new Date())
        .setDescription(
          "An internal error has occurred. Please contact the bot developer with the error code above if this error persists."
        );

      await this.rest.createMessage(ev.message.channelId, {
        content: `Error code: \`${id}\``,
        embeds: [embed],
        allowedMentions: {},
      });
    } catch (e) {
      log.error(`Error sending error message: ${e}`);
    }
  }
}

const idRegex = /^\d{15,}$/;
const mentionRegex = /^<[@#][&!]?(\d{15,})>$/;

export class Context {
  private _bot: Bot;
  private _rest: RestClient;

  private _message: Structures.Message;
  private _author: Structures.User | Structures.Member;

  private _guild?: Structures.Guild;
  private _channel: Structures.Channel;

  private _command: string;
  args: Array<string>;

  private _prefix: string;

  constructor(bot: Bot, ev: GatewayClientEvents.MessageCreate) {
    this._bot = bot;
    this._rest = bot.rest;

    this._message = ev.message;
    this._author = ev.message.member ?? ev.message.author;

    let _prefix: string | undefined;
    for (const prefix of bot.prefixes) {
      if (!this._message.content.toLowerCase().startsWith(prefix)) continue;

      _prefix = prefix;
    }

    if (!_prefix) throw NotCommandError;
    this._prefix = _prefix;

    this.args = this._message.content
      .slice(this._prefix.length)
      .trim()
      .split(/\s+/);

    if (this.args?.length === 0) throw NotCommandError;

    this._command = this.args.shift() ?? "";
    if (this._command === "") throw NotCommandError;

    this._guild = this._message.guildId
      ? bot.guilds.get(this._message.guildId)
      : undefined;

    const channel = bot.channels.get(this._message.channelId);
    if (!channel) {
      throw ChannelNotFound(this._message.channelId);
    }
    this._channel = channel;
  }

  get bot(): Bot {
    return this._bot;
  }

  get rest(): RestClient {
    return this._rest;
  }

  get message(): Message {
    return this._message;
  }

  get author(): Member | User {
    return this._author;
  }

  get guild(): Guild | undefined {
    return this._guild;
  }

  get channel(): Channel {
    return this._channel;
  }

  get rootCommand(): string {
    return this._command;
  }

  get prefix(): string {
    return this._prefix;
  }

  get rootChannel(): Channel | null {
    return this._bot.getRootChannel(this._channel.id);
  }

  get db(): Database {
    return db;
  }

  public async getGuildConfig(): Promise<GuildConfig | null> {
    return this._message.guildId
      ? await db.getGuildConfig(this._message.guildId)
      : null;
  }

  public embed(
    msg: string,
    title: string | undefined = undefined,
    color: number | undefined = undefined
  ): Structures.MessageEmbed {
    color = color ?? colours.BLURPLE;

    return {
      color,
      title,
      description: msg,
    } as MessageEmbed;
  }

  public async reply(
    msg: string,
    color: number | undefined = undefined
  ): Promise<Structures.Message> {
    return this._bot.rest.createMessage(this._channel.id, {
      embeds: [this.embed(msg, undefined, color)],
    });
  }

  public async react(emoji: string) {
    try {
      await this._rest.createReaction(
        this._message.channelId,
        this._message.id,
        emoji
      );
    } catch (e) {
      log.warn(`Error reacting to message ID ${this._message.id}: ${e}`);
    }
  }

  public async pageEmbeds(
    embeds: Embed[]
  ): Promise<Structures.Message | undefined> {
    if (embeds.length === 0) return;

    if (embeds.length === 1) {
      return await this._bot.rest.createMessage(this._channel.id, {
        embeds,
      });
    }

    const msg = await this._bot.rest.createMessage(this._channel.id, {
      embeds: [embeds[0]],
    });

    ["⬅️", "➡️"].forEach(async (e) => await msg.react(e));

    let page = 0;

    addReactionHandler(this._author.id, msg.id, "⬅️", async (_) => {
      let newPage = page - 1;

      if (newPage < 0) newPage = embeds.length - 1;
      page = newPage;

      await msg.edit({
        embeds: [embeds[newPage]],
      });
    });

    addReactionHandler(this._author.id, msg.id, "➡️", async (_) => {
      let newPage = page + 1;

      if (newPage > embeds.length - 1) newPage = 0;
      page = newPage;

      await msg.edit({
        embeds: [embeds[newPage]],
      });
    });

    return msg;
  }

  public parseChannel = (arg: string) => this._parseChannel(arg);

  public parseGuildChannel(arg: string): Structures.Channel | undefined {
    if (!this.message.guildId) return undefined;

    const channel = this._parseChannel(arg);
    if (!channel) return undefined;

    if (channel.guildId !== this.message.guildId) return undefined;
    return channel;
  }

  private _parseChannel(arg: string): Structures.Channel | undefined {
    if (idRegex.test(arg)) return this.bot.channels.get(arg);

    const matches = mentionRegex.exec(arg);
    if (!matches || matches.length < 2) return undefined;

    return this.bot.channels.get(matches[1]);
  }

  public parseRole(arg: string): Structures.Role | undefined {
    if (!this.guild) return undefined;

    if (idRegex.test(arg)) return this.guild.roles.get(arg);

    const matches = mentionRegex.exec(arg);
    if (!matches || matches.length < 2) return undefined;

    return this.guild.roles.get(matches[1]);
  }

  async parseMember(arg: string): Promise<Structures.Member | undefined> {
    if (!this.guild) return undefined;

    let id = arg;
    if (!idRegex.test(arg)) {
      const matches = mentionRegex.exec(arg);
      if (!matches || matches.length < 2) return undefined;

      id = matches[1];
    }

    try {
      return await this.guild.fetchMember(id);
    } catch (e) {
      return undefined;
    }
  }

  async parseUser(arg: string): Promise<Structures.User | undefined> {
    let id = arg;
    if (!idRegex.test(arg)) {
      const matches = mentionRegex.exec(arg);
      if (!matches || matches.length < 2) return undefined;

      id = matches[1];
    }

    try {
      return await this.rest.fetchUser(id);
    } catch (e) {
      return undefined;
    }
  }
}
