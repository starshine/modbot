import { GatewayClientEvents, Structures } from "detritus-client";

export class HandlerManager<K, V> {
  private _map: Map<K, V>;
  private _timeouts: Map<K, ReturnType<typeof setTimeout>>;

  constructor() {
    this._map = new Map<K, V>();
    this._timeouts = new Map<K, ReturnType<typeof setTimeout>>();
  }

  public set(key: K, value: V, timeout: number = 15 * 60) {
    this._map.set(key, value);

    this._timeouts.set(
      key,
      setTimeout(() => this._map.delete(key), timeout * 1000)
    );
  }

  public get(key: K): V | undefined {
    return this._map.get(key);
  }

  public delete(key: K) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    if (this._map.delete(key)) clearTimeout(this._timeouts.get(key)!);
  }
}

export interface ReactionKey {
  userId: string;
  messageId: string;
  emoji: string;
}

export interface ReactionValue {
  removeOnUse?: boolean;
  run(ev: GatewayClientEvents.MessageReactionAdd): Promise<void>;
}

export const reactionHandler = new HandlerManager<ReactionKey, ReactionValue>();

export function addReactionHandler(
  userId: string,
  messageId: string,
  emoji: string | Structures.Emoji,
  run: (ev: GatewayClientEvents.MessageReactionAdd) => Promise<void>,
  removeOnUse: boolean | undefined = false
): void {
  let emojiName = "";
  if (typeof emoji === "object") {
    emojiName = emoji.format;
  } else {
    emojiName = emoji;
  }

  reactionHandler.set(
    { userId, messageId, emoji: emojiName },
    { run, removeOnUse }
  );
}
