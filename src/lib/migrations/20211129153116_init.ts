import { Knex } from "knex";

export function up(knex: Knex) {
  return Promise.all([
    knex.schema.createTable("guilds", (table) => {
      table.text("id").primary().notNullable();
      table.text("helperRole").nullable();
      table.text("modRole").nullable();
      table.text("modLog").nullable();
    }),

    knex.schema.createTable("modlog", (table) => {
      table.increments("id").primary().notNullable();

      table
        .text("guildId")
        .notNullable()
        .references("id")
        .inTable("guilds")
        .onDelete("cascade");
      table.text("userId").notNullable();
      table.text("infractionType").notNullable();
      table.timestamp("timestamp", { useTz: false }).notNullable();
      table.text("reason").notNullable();
      table.text("moderatorId").notNullable();

      table.text("channelId").nullable();
      table.text("messageId").nullable();
    }),
  ]);
}

export function down(knex: Knex) {
  return Promise.all([
    knex.schema.dropTable("modlog"),
    knex.schema.dropTable("guilds"),
  ]);
}
