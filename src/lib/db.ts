import { join as joinPath, dirname } from "path";
import { fileURLToPath } from "url";

import knex, { Knex } from "knex";
import dotenv from "dotenv";

import { named } from "./log.js";
import { GuildConfig, ModLogEntry } from "./tables.js";
import { NoRowsError } from "./errors.js";

dotenv.config();

const DEFAULTURL = "postgresql://postgres:postgres@localhost/postgres";

const log = named("db");

const db = knex({
  client: "postgres",
  useNullAsDefault: true,
  connection: process.env.DATABASE_URL || DEFAULTURL,
});

const __dirname = dirname(fileURLToPath(import.meta.url));

db.migrate
  .latest({
    directory: joinPath(__dirname, "migrations"),
  })
  .then(() => log.info("Finished database migrations!"));

async function getFirst<T = any>(
  fn: (knex: Knex) => Knex.QueryBuilder<T>
): Promise<T> {
  const res = await fn(db);

  if (!res || (res.length || 0) === 0) throw NoRowsError;

  return res[0];
}

async function getGuildConfig(id: string): Promise<GuildConfig> {
  return await getFirst((knex) =>
    knex<GuildConfig>("guilds")
      .insert({ id })
      .onConflict("id")
      .merge()
      .returning("*")
  );
}

async function getModLogFor(
  guildId: string,
  userId: string
): Promise<ModLogEntry[]> {
  return await db<ModLogEntry>("modlog")
    .where("guildId", guildId)
    .where("userId", userId);
}

async function setEntryIds(
  entry: number,
  channelId: string,
  messageId: string
) {
  await db<ModLogEntry>("modlog")
    .update({ channelId, messageId })
    .where("id", entry);
}

export interface Database {
  knex: Knex;

  getGuildConfig(id: string): Promise<GuildConfig>;
  getModLogFor(guildId: string, userId: string): Promise<ModLogEntry[]>;
  setEntryIds(
    entry: number,
    channelId: string,
    messageId: string
  ): Promise<void>;
}

export default {
  knex: db,

  getGuildConfig,
  getModLogFor,
  setEntryIds,
} as Database;
