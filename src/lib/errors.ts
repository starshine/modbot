export const UserError = Error;

export const ChannelNotFound = (channelId: string) =>
  new Error(`No channel with ID ${channelId} found.`);

export const NotInGuild = new Error("Not in guild");
export const NotCommandError = new Error(
  "Can't create a context, not a command message."
);
export const NoRowsError = new Error("No rows in query result");
