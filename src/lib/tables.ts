export interface GuildConfig {
  id: string;

  helperRole?: string;
  modRole?: string;

  modLog?: string;
}

export interface ModLogEntry {
  id: number;
  guildId: string;
  hid: number;
  userId: string;
  moderatorId: string;

  infractionType: string;
  timestamp: Date;
  reason: string;

  channelId?: string;
  messageId?: string;
}
