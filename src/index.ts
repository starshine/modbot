import path, { dirname } from "path";
import { fileURLToPath } from "url";
import fs from "fs";

import dotenv from "dotenv";

import { Bot, Command } from "./lib/bot.js";
import { named } from "./lib/log.js";

const log = named("init");

dotenv.config();

const bot = new Bot(process.env.TOKEN!, {
  prefixes: process.env.PREFIXES?.split(",") ?? ["."],
  owners: process.env.OWNERS?.split(","),
});

let cmdCount = 0;

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const files = fs.readdirSync(path.join(__dirname, "/cmds"));

Promise.all(
  files.map(async (file) => {
    const raw = await import(path.join(__dirname, "/cmds/", file));
    const cmd = raw.default as Command;

    cmdCount += 1;

    bot.commands.set(cmd.name, cmd);
    cmd.aliases?.forEach((alias) => bot.commands.set(alias, cmd));

    log.debug(`Loaded command ${cmd.name}`);
  })
).then(() => {
  log.debug(`Loaded ${cmdCount} command${cmdCount === 1 ? "" : "s"}!`);
});

bot.run();
