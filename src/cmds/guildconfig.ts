import { stripIndents } from "common-tags";

import { Command } from "../lib/bot.js";
import colours from "../lib/colours.js";
import { GuildConfig } from "../lib/tables.js";

const cmd: Command = {
  name: "guildconfig",
  aliases: ["guild-config"],
  summary: "Manage this guild's configuration",
  modOnly: true,

  async run(ctx) {
    if (ctx.args.length === 0) {
      const config = await ctx.getGuildConfig();
      if (!config) throw new Error("Not in a guild!");

      return [
        ctx.embed(
          stripIndents`
        Helper role: ${
          config.helperRole ? `<@&${config.helperRole}>` : "Not set"
        }
        Mod role: ${config.modRole ? `<@&${config.modRole}>` : "Not set"}
        Mod log channel: ${config.modLog ? `<#${config.modLog}>` : "Not set"}
        `,
          `Configuration for ${ctx.guild!.name}`
        ),
      ];
    }

    if (ctx.args.length !== 2)
      return [
        ctx.embed(
          "You need to give both a key to edit and a channel/role mention.",
          undefined,
          colours.RED
        ),
      ];

    switch (ctx.args[0]) {
      case "helperrole":
      case "helper-role": {
        const role = ctx.parseRole(ctx.args[1]);
        if (!role)
          return [
            ctx.embed(
              `No role with ID \`${ctx.args[1]}\` found.`,
              undefined,
              colours.RED
            ),
          ];

        await ctx.db
          .knex<GuildConfig>("guilds")
          .where("id", ctx.guild!.id)
          .update({ helperRole: role.id });

        return [ctx.embed(`Set this guild's helper role to ${role.mention}!`)];
      }
      case "modrole":
      case "mod-role": {
        const role = ctx.parseRole(ctx.args[1]);
        if (!role)
          return [
            ctx.embed(
              `No role with ID \`${ctx.args[1]}\` found.`,
              undefined,
              colours.RED
            ),
          ];

        await ctx.db
          .knex<GuildConfig>("guilds")
          .where("id", ctx.guild!.id)
          .update({ modRole: role.id });

        return [ctx.embed(`Set this guild's mod role to ${role.mention}!`)];
      }
      case "modlog":
      case "mod-log": {
        const channel = ctx.parseGuildChannel(ctx.args[1]);
        if (!channel)
          return [
            ctx.embed(
              `No channel with ID \`${ctx.args[1]}\` found.`,
              undefined,
              colours.RED
            ),
          ];

        await ctx.db
          .knex<GuildConfig>("guilds")
          .where("id", ctx.guild!.id)
          .update({ modLog: channel.id });

        return [
          ctx.embed(`Set this guild's mod log channel to ${channel.mention}!`),
        ];
      }
    }

    return [
      ctx.embed(
        `\`${ctx.args[0]}\` isn't a valid configuration key.`,
        undefined,
        colours.RED
      ),
    ];
  },
};

export default cmd;
