import { Embed } from "detritus-client/lib/utils/index.js";
import { Command } from "../lib/bot.js";

const cmd: Command = {
  name: "pages",
  summary: "Hello",

  async run(ctx) {
    const embeds: Embed[] = [];

    ["Page 1", "Page 2", "Page 3", "Page 4"].forEach((s) => {
      embeds.push(new Embed().setDescription(s));
    });

    await ctx.pageEmbeds(embeds);
  },
};

export default cmd;
