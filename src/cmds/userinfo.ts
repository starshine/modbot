import { Member } from "detritus-client/lib/structures/index.js";
import { Embed } from "detritus-client/lib/utils/index.js";

import { Command, Context } from "../lib/bot.js";

async function runUser(ctx: Context): Promise<Embed[] | string> {
  const user = await ctx.parseUser(ctx.args[0]);
  if (!user) return "User not found.";

  return [
    new Embed()
      .setAuthor(user.tag, user.avatarUrl)
      .setDescription(user.id)
      .setFooter(`User ID: ${user.id}`)
      .setTimestamp(new Date())
      .setThumbnail(user.avatarUrl)
      .addField("User information for", user.mention, false)
      .addField("Avatar", `[Link](${user.avatarUrl})`, true)
      .addField("Username", user.tag, true)
      .addField("Created at", `<t:${(user.createdAtUnix / 1000).toFixed(0)}>`),
  ];
}

const cmd: Command = {
  name: "userinfo",
  aliases: ["i", "whois"],
  summary: "Get info about a member or user",
  guildOnly: true,

  async run(ctx) {
    let member: Member | undefined = ctx.author as Member;
    if (member) member.user = ctx.message.author;

    if (ctx.args.length > 0) {
      member = await ctx.parseMember(ctx.args[0]);
    }
    if (!member) {
      return runUser(ctx);
    }

    const embed = new Embed()
      .setAuthor(member.tag, member.avatarUrlFormat("png"))
      .setThumbnail(member.avatarUrl)
      .setDescription(member.id)
      .setFooter(`User ID: ${member.id}`)
      .setTimestamp(new Date())
      .setColor(member.color !== 0 ? member.color : 0x7289da)
      .addField("User information for", member.mention, false);

    return [embed];
  },
};

export default cmd;
