import { Structures } from "detritus-client";
import { Embed } from "detritus-client/lib/utils/index.js";
import { stripIndents } from "common-tags";

import { ModLogEntry } from "../lib/tables.js";
import { Context, Command } from "../lib/bot.js";
import { named } from "../lib/log.js";
import colours from "../lib/colours.js";

const log = named("warn");

function logEmbed(ctx: Context, entry: ModLogEntry, member: Structures.Member) {
  return new Embed()
    .setTitle(`Warning - Case ${entry.id}`)
    .setColor(colours.ORANGE)
    .setDescription(
      stripIndents`
        **Offender:** ${member.tag} (${member.id})
        **Moderator:** ${ctx.author.tag} (${ctx.author.id})
        **Reason:** ${entry.reason}
    `
    )
    .setTimestamp(entry.timestamp)
    .setFooter(`User ID: ${member.id}`);
}

const cmd: Command = {
  name: "warn",
  summary: "Warn a user",
  helperOnly: true,

  async run(ctx) {
    if (ctx.args.length < 2)
      return "You need to give both a user and a reason.";

    const member = await ctx.parseMember(ctx.args.shift()!);
    if (!member) return "No member matching that ID or mention found.";

    if (member.user.id === ctx.bot.user?.id)
      return "After all my hard work, *this* is how you reward me? What a disgrace.";

    const modRoles = (ctx.author as Structures.Member).roles
      .sort((role) => role?.position ?? 0)
      .reverse();
    const userRoles = member.roles
      .sort((role) => role?.position ?? 0)
      .reverse();

    if ((modRoles[0]?.position || 0) <= (userRoles[0]?.position || 0))
      return "You're not high enough in the role hierarchy to do that.";

    const reason = ctx.args.join(" ");

    const entry = (
      await ctx.db
        .knex<ModLogEntry>("modlog")
        .insert({
          guildId: ctx.message.guildId!,
          userId: member.user.id,
          infractionType: "warn",
          timestamp: new Date(),
          reason,
          moderatorId: ctx.author.id,
        })
        .returning("*")
    )[0];

    if (!entry)
      return "Error saving warning in database, warning has not been logged.";

    const config = await ctx.getGuildConfig();
    if (config?.modLog) {
      try {
        const logMsg = await ctx.rest.createMessage(config.modLog, {
          embeds: [logEmbed(ctx, entry, member)],
        });

        await ctx.db.setEntryIds(entry.id, config.modLog, logMsg.id);
      } catch (e) {
        log.error(`Error sending log embed for warning #${entry.id}: ${e}`);
        await ctx.rest.createMessage(ctx.message.channelId, {
          content: `There was an error sending the log embed: ${e}`,
          allowedMentions: {},
        });
      }
    }

    try {
      const dmChannel = await member.createOrGetDm();
      await dmChannel.createMessage(
        `You were warned in ${ctx.guild!.name}\nReason: ${reason}`
      );
    } catch (e) {
      await ctx.react("⚠️");
      return "I was unable to send the user a DM notifying them of their warning.";
    }

    await ctx.react("✅");

    return [
      {
        color: colours.ORANGE,
        title: `${member.tag} was warned`,
        description: stripIndents`
        **Case ID:** ${entry.id}
        **Moderator:** ${ctx.author.mention}
        **Reason:** ${reason}
      `,
      },
    ] as Structures.MessageEmbed[];
  },
};

export default cmd;
