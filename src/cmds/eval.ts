import { Command } from "../lib/bot.js";
import colours from "../lib/colours.js";
import { named } from "../lib/log.js";

const log = named("eval");

const _eval: Command = {
  name: "eval",
  summary: "Execute arbitrary code",
  ownerOnly: true,

  async run(ctx): Promise<null> {
    let code = ctx.message.content
      .slice(ctx.prefix.length)
      .trim()
      .slice(ctx.rootCommand.length)
      .trim();

    if (code.startsWith("```")) code = code.slice(3);
    if (code.endsWith("```")) code = code.substring(0, code.length - 3);
    if (code.startsWith("js")) code = code.slice(2);

    log.warn(
      `User ${ctx.author.tag} (${ctx.author.id}) used eval command in ${ctx.channel.id} (#${ctx.channel.name}):`
    );
    log.warn("```\n" + code + "\n```");

    try {
      // eslint-disable-next-line no-eval
      const res = eval(code);

      if (res) await ctx.reply(`\`\`\`js\n${res}\n\`\`\``);
      try {
        await ctx.bot.rest.createReaction(
          ctx.message.channelId,
          ctx.message.id,
          "✅"
        );
      } catch (e) {
        // ignore
      }
    } catch (e) {
      await ctx.reply(`\`\`\`js\n${e}\n\`\`\``, colours.RED);

      try {
        await ctx.bot.rest.createReaction(
          ctx.message.channelId,
          ctx.message.id,
          "❌"
        );
      } catch (e) {
        // ignore
      }
    }

    return null;
  },
};

export default _eval;
