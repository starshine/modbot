import { Context, Command } from "../lib/bot.js";

const ping: Command = {
  name: "ping",
  summary: "Show the bot latency!",

  async run(ctx: Context): Promise<string> {
    const { gateway, rest } = await ctx.bot.ping();

    return `Pong! Gateway: ${gateway}ms, API: ${rest}ms`;
  },

  subCommands: [
    {
      name: "pong",
      summary: "Table tennis!",

      async run(_): Promise<string> {
        return "🏓";
      },
    },
  ],
};

export default ping;
