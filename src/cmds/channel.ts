import { Command } from "../lib/bot.js";

const channel: Command = {
  name: "channel",
  summary: "Show the bot latency!",

  async run(ctx): Promise<string> {
    const conf = await ctx.getGuildConfig();

    return conf?.modLog
      ? `The mod log channel is <#${conf.modLog}>`
      : "There is no mod log channel set.";
  },
};

export default channel;
